package jc.example.com.tp_startactivitywithresult;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    /*
     * Ici aussi, l'exemple est un cas particulier que l'on pourrait traiter de façon plus simple,
     * mais il montre comment gérer la valeur de retour d'une activité
     *
     */

    private static final int CODE_ACTIVITE_QUESTIONNAIRE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button) findViewById(R.id.Btn_Question))
                .setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        // Toast.makeText(MainActivity.this, "coucou",
                        // Toast.LENGTH_SHORT).show();
                        Intent questionnaire = new Intent(MainActivity.this,
                                QuestionActivity.class);
                        startActivityForResult(questionnaire,
                                CODE_ACTIVITE_QUESTIONNAIRE);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            // traitement du retour du questionnaire
            case CODE_ACTIVITE_QUESTIONNAIRE:
                switch (resultCode) {
                    case RESULT_OK:
                        Toast.makeText(this, "Validé", Toast.LENGTH_SHORT).show();
                        return;
                    case RESULT_CANCELED:
                        Toast.makeText(this, "Refusé", Toast.LENGTH_SHORT).show();
                        return;

                    default:
                        return;
                }

            default:
                return;
        }
    }


}
