package jc.example.com.tp_startactivitywithresult;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class QuestionActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
    }

    // solution qui fonctionne, mais "pas propre" en IHM !!!
    // (on peut aussi renvoyer des données comme pour un passage de paramètres :
    // this.getIntent().putExtra.....)

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOui:
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.btnNon:
                setResult(RESULT_CANCELED);
                finish();
                break;
            default:
                // par défaut, l'activité renvoie toujours "canceled"
                // setResult(RESULT_CANCELED);
                break;
        }

    }
}
